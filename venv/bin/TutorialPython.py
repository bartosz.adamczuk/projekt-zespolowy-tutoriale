from array import array
import random
import useful_tools #stworzenie wlasnego modulu MUSI ZNAJDOWAC SIE W KATALOGU LIB
from Student import Student #Stworzenie wlasnej klasy
from Question import Pytanie #Do quizu
from Chef import Chef
from ChineseChef import ChineseChef

def kalkulator(num1,znak,num2):
     if znak== "+":
         return num1 + num2
     elif znak == "-":
         return num1-num2
     elif znak == "*":
         return num1*num2
     elif znak == "/":
         return num1/num2
     else:
         return "ZŁY ZNAK"

def listy(): #moga przetrzymywac rozne type zmiennych
    lucky_numbers = [2,6,8,16,25]
    koledzy = ["Pawel","Mateusz","Patryk"]
    print(koledzy[0])
    print(koledzy[1][4])
    koledzy.append("Damian")
    print(koledzy)
    koledzy.remove("Mateusz")
    koledzy.sort()
    print(koledzy[1:])
    koledzy.extend(lucky_numbers)
    koledzy.insert(2,6)
    print(koledzy.index("Damian"))
    print(koledzy.count(6))
    koledzy2 = koledzy.copy()
    print(koledzy2)

def zmienne(): #laczenie roznych zmiennych
    imie = input("Podaj imie ")
    wiek = 50
    print("Zyl sobie "+ imie)
    print("mial "+ str(wiek) + "lat")
    print("Dlugosc imienia " + str(len(imie)))

def krotki(): #nie moga byc zmieniane po zadeklarowaniu
    wspolrzedne = (4,5)
    print(wspolrzedne[0])
    print(wspolrzedne.count(4))
    listakrotek = [(1,2),(3,4)]
    print(listakrotek[0])
    print(listakrotek[1][1])

def wypisz(tekst):
    print(tekst)

def szescian(liczba):
    return liczba*liczba*liczba

def ify():
    is_male = False
    is_tall = False
    if is_male and is_tall:
        print("Jestes wysokim mezczyzna")
    elif is_male and not(is_tall):
        print("Jestes niskim mezczyzna")
    elif not(is_male) and is_tall:
        print("Jestes wysoka kobieta")
    else:
        print("Jestes niska kobieta")

def ify2(num1,num2,num3):
    if num1 >=num2 and num1 >= num3:
        return num1
    elif num2>=num1 and num2>=num3:
        return num2
    else:
        return num3

def slownik():
    miesiace = {
        "Sty":"Styczen",
        "Lut":"Luty",
        "Mar":"Marzec"
    }
    print(miesiace["Sty"])

def petla_while():
    zgadywana = random.randint(1,101)
    proby = 3
    zgadles=True
    while zgadles:
        propozycja = float(input("Podaj propozycje"))
        if propozycja == zgadywana:
            print("Brawo! Zgadles")
            zgadles=False
        elif propozycja > zgadywana:
            print("Za duza")
        else:
            print("Za mala")
        proby-=1
        if proby == 0:
            print("Wykorzystales swoje proby")
            zgadles = False

def petla_for():
    for letter in "Ucze sie Pythona":
        print(letter)

    koledzy = ["Mateusz","Pawel","Damian"]
    for kolega in koledzy:
        print(kolega)
    for licznik in range(3,10):
        print(licznik)

def podnoszenie_do_potegi(liczba,potega):
    return liczba**potega

def macierze():
    macierz = [
        [1,2,3],
        [4,5,6],
        [7,8,9],
        [0]
    ]
    print(macierz[3][0])
    for wiersz in macierz:
        for kolumna in wiersz:
            print(kolumna)

def translator(tekst):
    translacja = ""
    for letter in tekst:
        if letter.lower() in "aeiou":
            if letter.isupper():
                translacja =translacja+"G"
            else:
                translacja=translacja + "g"
        else:
            translacja=translacja + letter
    return translacja

def try_except():
    try:
        value = 10/0
        liczba=int(input("Podaj liczbe: "))
        print(liczba)
    except ZeroDivisionError as err:
        print(err)
    except ValueError:
        print("Miala byc liczba")

def czytanie_pliku():
    pracownicy_plik = open("../../Pracownicy.txt","r")
    for pracownik in pracownicy_plik.readlines():
        print(pracownik)
    pracownicy_plik.close()

def dopisanie_do_pliku():
    pracownicy_plik = open("../../Pracownicy.txt","a")#a oznacza append czyli dodanie na koncu
    pracownicy_plik.write("\nPawel - Sprzedawca")
    pracownicy_plik.close()

def nadpisanie_pliku():
    pracownicy_plik = open("../../index.html", "w")
    pracownicy_plik.write("<p>To jest HTML</p>")
    pracownicy_plik.close()

def klasy():
    student1 = Student("Bartek", "Adamczuk","Informatyka",3)
    student2 = Student("Mateusz", "Gotowicki","Informatyka",3)
    print(student1.polowa_studiow())

def Quiz():
    pytania = [
        "Jakiego koloru jest banan?\n(a)Zolty\n(b)Czerwony\n(c)Niebieski\n\n",
        "Jakiego koloru jest truskawka?\n(a)Zolty\n(b)Czerwony\n(c)Niebieski\n\n",
        "Jakiego koloru jest jagoda?\n(a)Zolty\n(b)Czerwony\n(c)Niebieski\n\n",
    ]
    odpowiedzi = [
        Pytanie(pytania[0], "a"),
        Pytanie(pytania[1], "b"),
        Pytanie(pytania[2], "c")
    ]
    punkty =0
    for odpowiedz in odpowiedzi:
        propozycja = input(odpowiedz.pytanie)
        if propozycja == odpowiedz.odpowiedz:
            punkty += 1
    print(punkty)

def dziedziczenie():#klasa chef
    mychef = Chef()
    mychef.make_chicken()
    myChinesechef = ChineseChef()
    myChinesechef.make_special_dish()

#zmienne()
#listy()
#krotki()
#wypisz("Ucze sie pythona!")
#print(szescian(2))
#ify()
#print("Najwieksza jest liczba " +str(ify2(11,2,6)))
#print(kalkulator(1,"-",2))
#slownik()
#petla_while()
#petla_for()
#print(podnoszenie_do_potegi(2,10))
#macierze()
#print(translator(input("Podaj tekst ")))
#try_except()
#czytanie_pliku()
#dopisanie_do_pliku()
#nadpisanie_pliku()
#klasy()
#Quiz()
dziedziczenie()